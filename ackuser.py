#!/usr/bin/env python3
#
# SPDX-Licences-Identifier: MIT

import ackuser.cli

if __name__ == "__main__":
    ackuser.cli.ackuser()
