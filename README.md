# ackuser

A bot to mark external Gitlab users as no longer external. This bot is
primarily invoked from other bots but it can be invoked manually for other
tasks.

## How it works

`ackuser`, together with [hookiedookie](https://gitlab.freedesktop.org/freedesktop/hookiedookie/)
is already available on https://gitlab.freedesktop.org.

In the freedesktop.org GitLab instance, new users are marked as "external" and
required to file an account verification bug. This was introduced to filter out
spam and reduce malicious users accounts. New user requests are
approved manually but the GitLab UI to uncheck the "external" setting is
convoluted.

`ackuser` makes this simpler by automating this task - a freedesktop.org GitLab
Admin only needs to apply the "user::approved" label to the bug and this bot
takes care of the "external" setting, a short welcome message and closing the bug.

### Tokens

`ackuser` requires a GitLab access token with API access, either provided
in `$XDG_CONFIG_HOME/ackuser/userbot.token` or via one of the `--token-file` or
`--token-env` commandline arguments. The token needs to have access to the
`freedesktop/freedesktop` project and be able to change users settings, i.e. it
needs to be an Administrator level token.

This token *should* be created for the **@userbot** user to impersonate the user
approval bot.

## Manual approval via an issue link

```
$ pip install git+https://gitlab.freedesktop.org/freedesktop/ackuser
$ ackuser approve https://gitlab.freedesktop.org/freedesktop/freedesktop/-/issues/123
```

## Testing locally

You *really* should use `--readonly` to avoid making any actual changes. All
actions that the bot would but doesn't do are prefixed with `[RO SKIP]` in the
logs.

To test the CLI for a specific issue, run the following command:

```
$ ./ackuser.py \
   --readonly \
   --verbose \
   approve \
   https://gitlab.freedesktop.org/freedesktop/freedesktop/-/issues/123
```

The above runs *in readonly* mode and approves the user that filed the given issue.
